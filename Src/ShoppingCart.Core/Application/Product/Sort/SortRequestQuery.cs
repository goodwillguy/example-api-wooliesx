using MediatR;
using ShoppingCart.Api.Contracts;
using System.Collections.Generic;

namespace ShoppingCart.Core.Application.Product.Sort
{
    public class SortRequestQuery : SortRequest, IRequest<IEnumerable<ProductResponse>>
    {
    }
}