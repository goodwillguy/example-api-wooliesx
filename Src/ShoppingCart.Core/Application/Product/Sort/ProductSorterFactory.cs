using ShoppingCart.Core.Entity;
using ShoppingCart.Core.Entity.Sorter;
using ShoppingCart.Core.Interface;
using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Application.Product.Sort
{
    internal class ProductSorterFactory
    {
        private readonly IShoppingHistoryService _shoppingHistoryService;

        public ProductSorterFactory(IShoppingHistoryService shoppingHistoryService)
        {
            _shoppingHistoryService = shoppingHistoryService;
        }

        internal async Task<ProductSorter> GetProductSorter(System.Collections.Generic.IEnumerable<Entity.Product> products, SortRequestQuery request)
        {
            switch (request.SortOption)
            {
                case Api.Contracts.SortBy.Ascending:
                    return new NameSorter(products, SortDirection.Ascending);
                case Api.Contracts.SortBy.Descending:
                    return new NameSorter(products, SortDirection.Descending);
                case Api.Contracts.SortBy.High:
                    return new PriceSorter(products, SortDirection.Descending);
                case Api.Contracts.SortBy.Recommended:
                    IEnumerable<ShoppingHistoryValueObject> shoppingHistories = await _shoppingHistoryService.GetAllShoppingHistory();

                    var popularityCalculator = new PopularityCalculator(shoppingHistories);

                    var productWithPopularity = products.ToList();
                    foreach (var product in productWithPopularity)
                    {
                        product.SetPopularity(popularityCalculator);
                    }

                    return new RecommendedSorter(productWithPopularity);
                default:
                    return new PriceSorter(products, SortDirection.Ascending);
            }
        }
    }
}