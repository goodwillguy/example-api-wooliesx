using MediatR;
using ShoppingCart.Api.Contracts;
using ShoppingCart.Core.Entity.Sorter;
using ShoppingCart.Core.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Application.Product.Sort
{
    internal class SortRequestQueryHandler : IRequestHandler<SortRequestQuery, IEnumerable<ProductResponse>>
    {
        public IProductService _productService { get; }

        private readonly ProductSorterFactory _productSorterFactory;

        public SortRequestQueryHandler(IProductService productService, ProductSorterFactory productSorterFactory)
        {
            _productService = productService;
            _productSorterFactory = productSorterFactory;
        }

        public async Task<IEnumerable<ProductResponse>> Handle(SortRequestQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<Entity.Product> products = await _productService.GetAllProducts();

            ProductSorter sorter = await _productSorterFactory.GetProductSorter(products, request);

            return sorter
                    .Sort()
                    .Select(p => new ProductResponse { Name = p.Name, Price = p.Price, Quantity = p.Quantity });
        }
    }
}
