using MediatR;
using Microsoft.Extensions.Options;
using ShoppingCart.Api.Contracts;
using ShoppingCart.Core.Common;
using ShoppingCart.Core.User.Get;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Application.User.Get
{
    internal class UserRequestHandler : IRequestHandler<UserRequest, UserResponse>
    {
        private UserSettings _userSettings;

        public UserRequestHandler(IOptions<UserSettings> userSettingsOptions)
        {
            _userSettings = userSettingsOptions.Value;
        }

        public async Task<UserResponse> Handle(UserRequest request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(_userSettings.Token))
            {
                throw new InvalidOperationException("User setting is not configured");
            }

            // ToDo: Currently user is obtained from settings, get from Identity Server later

            return new UserResponse { Name = _userSettings.Name, Token = _userSettings.Token };
        }
    }
}
