using MediatR;
using ShoppingCart.Api.Contracts;

namespace ShoppingCart.Core.User.Get
{
    /// <summary>
    // Get user information
    /// </summary>
    public class UserRequest : IRequest<UserResponse>
    {
    }
}
