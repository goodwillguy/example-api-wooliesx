namespace ShoppingCart.Core.Application.Trolley
{
    using ShoppingCart.Core.Entity;
    using ShoppingCart.Core.ValueObject;
    using System.Collections.Generic;
    using System.Linq;

    internal class Mapper
    {
        internal static Trolley MapToTrolley(TrolleyCommand request)
        {
            var products = MapToProduct(request);
            return new Trolley(products);
        }

        internal static IEnumerable<Product> MapToProduct(TrolleyCommand request)
        {
            return request.Products.Select(p => new Product(p.Name, p.Price, request.Quantities.FirstOrDefault(q => q.Name.ToLower() == p.Name.ToLower())?.Quantity ?? 0));
        }

        internal static IEnumerable<Promotion> MapToPromotions(TrolleyCommand request)
        {
            return request
                    .Specials
                    .Select(sp => new Promotion
                    (
                        sp.Quantities.Select(q => new QuantityValueObject { Product = q.Name, Quantity = q.Quantity }),
                        sp.Total
                    ));
        }
    }
}