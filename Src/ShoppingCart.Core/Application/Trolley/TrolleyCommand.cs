using MediatR;
using ShoppingCart.Api.Contracts;

namespace ShoppingCart.Core.Application.Trolley
{
    public class TrolleyCommand : TrolleyTotalRequest, IRequest<decimal>
    {
    }
}
