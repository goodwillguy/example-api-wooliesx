using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Application.Trolley
{
    public class TrolleyCommandHandler : IRequestHandler<TrolleyCommand, decimal>
    {
        public async Task<decimal> Handle(TrolleyCommand request, CancellationToken cancellationToken)
        {
            var trolley = Mapper.MapToTrolley(request);

            var promotions = Mapper.MapToPromotions(request);

            return trolley.Total(promotions);
        }
    }
}
