using FluentValidation;
using ShoppingCart.Api.Contracts;

namespace ShoppingCart.Core.Application.Trolley
{
    public class TrolleyCommandValidator : AbstractValidator<TrolleyCommand>
    {
        public TrolleyCommandValidator()
        {
            RuleFor(p => p.Products)
                .NotNull()
                .WithMessage("Products is required");

            RuleForEach(p => p.Products).SetValidator(new ProductValidator());

            RuleFor(p => p.Quantities)
                .NotNull()
                .WithMessage("Quantities Required");

            RuleForEach(p => p.Quantities).SetValidator(new QuantitiesValidator());
        }
    }

    internal class QuantitiesValidator : AbstractValidator<ProductQuantity>
    {
        public QuantitiesValidator()
        {
            RuleFor(q => q.Name)
                .NotNull()
                .WithMessage("Name is required");
        }
    }

    internal class ProductValidator : AbstractValidator<ProductData>
    {
        public ProductValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .WithMessage("Product name is required");

        }
    }
}
