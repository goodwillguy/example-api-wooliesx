using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShoppingCart.Core.Application.Product.Sort;
using ShoppingCart.Core.Common;
using ShoppingCart.Core.Interface;
using System.Reflection;

namespace ShoppingCart.Core.Configuration
{
    public static class ConfigurationExtension
    {
        /// <summary>
        /// Configure service dependencies
        /// </summary>
        /// <param name="services">service collection</param>
        /// <param name="configuration">configuration</param>
        /// <returns>mediatr services</returns>
        public static IServiceCollection AddCoreServiceConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            AssemblyScanner
                .FindValidatorsInAssembly(typeof(ConfigurationExtension).GetTypeInfo().Assembly)
                .ForEach(result => { services.AddScoped(result.InterfaceType, result.ValidatorType); });

            return services
                    .AddSingleton<ICachePolicy, CachePolicy>()
                    .AddSingleton(typeof(ICache<>), typeof(Cache<>))
                    .Configure<CacheSettings>(configuration.GetSection("CacheSettings"))
                    .Configure<CachePolicySettings>(configuration.GetSection("CachePolicySettings"))
                    .Configure<UserSettings>(configuration.GetSection("UserInfo"))
                    .AddMediatR(typeof(ConfigurationExtension).GetTypeInfo().Assembly)
                    .AddScoped(typeof(ProductSorterFactory))
                    .AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehaviour<,>));
        }
    }
}