using System.Collections.Generic;
using System.Linq;

namespace ShoppingCart.Core.Entity.Sorter
{
    /// <summary>
    /// Sort product by popularity
    /// </summary>
    internal class RecommendedSorter : ProductSorter
    {
        public RecommendedSorter(IEnumerable<Product> products) : base(products)
        {
        }

        internal override IEnumerable<Product> Sort()
        {
            // Popularity will be in ascending. 1 is more popular that 5

            return _products.OrderBy(p => p.Popularity ?? int.MaxValue);
        }
    }
}
