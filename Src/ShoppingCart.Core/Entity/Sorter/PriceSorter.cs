using System.Collections.Generic;
using System.Linq;

namespace ShoppingCart.Core.Entity.Sorter
{
    /// <summary>
    /// Sort products by price
    /// </summary>
    internal class PriceSorter : ProductSorter
    {
        internal SortDirection SortDirection { get; private set; }
        System.Func<Product, decimal> keySelector = p => p.Price;

        public PriceSorter(IEnumerable<Product> products, SortDirection sortDirection) : base(products)
        {
            SortDirection = sortDirection;
        }

        internal override IEnumerable<Product> Sort()
        {
            return SortDirection == SortDirection.Ascending ? _products.OrderBy(keySelector) : _products.OrderByDescending(p => p.Price);
        }
    }
}
