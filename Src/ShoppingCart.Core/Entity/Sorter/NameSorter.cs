using System.Collections.Generic;
using System.Linq;

namespace ShoppingCart.Core.Entity.Sorter
{
    /// <summary>
    /// Sort products by name
    /// </summary>
    internal class NameSorter : ProductSorter
    {
        internal SortDirection SortDirection { get; private set; }
        System.Func<Product, string> keySelector = p => p.Name;

        public NameSorter(IEnumerable<Product> products, SortDirection sortDirection) : base(products)
        {
            SortDirection = sortDirection;
        }

        internal override IEnumerable<Product> Sort()
        {
            return SortDirection == SortDirection.Ascending ? _products.OrderBy(keySelector) : _products.OrderByDescending(keySelector);
        }
    }
}
