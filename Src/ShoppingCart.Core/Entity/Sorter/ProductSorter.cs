using System.Collections.Generic;

namespace ShoppingCart.Core.Entity.Sorter
{
    /// <summary>
    /// Product Sorter
    /// </summary>
    internal abstract class ProductSorter
    {
        protected readonly IEnumerable<Product> _products;

        protected ProductSorter(IEnumerable<Product> products)
        {
            _products = products;

            // TODO : check if product need to be always present
            // Better to design system with resilience

            if (_products == null)
            {
                _products = new List<Product>();
            }
        }

        /// <summary>
        /// Sort products
        /// </summary>
        /// <returns></returns>
        internal abstract IEnumerable<Product> Sort();
    }
}
