namespace ShoppingCart.Core
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }
}