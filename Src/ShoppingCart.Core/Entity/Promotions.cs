using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingCart.Core.Entity
{
    public class Promotion
    {
        public IEnumerable<QuantityValueObject> Promotions { get; private set; }
        public decimal Total { get; private set; }

        public Promotion(IEnumerable<QuantityValueObject> promotions, decimal total)
        {
            Promotions = promotions;
            Total = total;

            if (Promotions == null)
            {
                Promotions = new List<QuantityValueObject>();
            }
        }

        public decimal CalculateDiscount(Trolley trolleys)
        {
            if (trolleys == null || trolleys.Products == null || !Promotions.Any() || !trolleys.Products.Any())
            {
                return 0;
            }

            // discount formula = (Prodct.Price * MinimumBatchQuantity * DiscountQuantity) - (DiscountedTotal *MinimumBatchQuantity)
            var discountPRoducts = Promotions
                  .Select(p => new
                  {
                      Product = trolleys
                                  .Products
                                  .FirstOrDefault(pr => pr.Name.ToLower() == p.Product.ToLower()),
                      DiscountQuatity = p.Quantity
                  })
                  .Where(p => p.DiscountQuatity > 0);


            if (!discountPRoducts.Any())
            {
                return 0;
            }

            int minBatchDiscount = discountPRoducts.Min(m => (int)m.Product.Quantity / m.DiscountQuatity);

            if (minBatchDiscount == 0)
            {
                return 0;
            }

            var total = 0.0m;
            foreach (var d in discountPRoducts)
            {
                total += d.Product.Price * minBatchDiscount * d.DiscountQuatity;
            }

            return total - (Total * minBatchDiscount);
        }
    }
}
