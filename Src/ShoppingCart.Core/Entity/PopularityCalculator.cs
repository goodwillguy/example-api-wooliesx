using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingCart.Core.Entity
{
    /// <summary>
    /// Calculate the popularity number
    /// </summary>
    public class PopularityCalculator
    {
        private readonly IEnumerable<ShoppingHistoryValueObject> _shoppingHistory;
        private List<string> _productByPopularity;

        public PopularityCalculator(IEnumerable<ShoppingHistoryValueObject> shoppingHistory)
        {
            _shoppingHistory = shoppingHistory;

            if (_shoppingHistory == null)
            {
                _shoppingHistory = new List<ShoppingHistoryValueObject>();
            }

            CalculatePopularity();
        }

        private void CalculatePopularity()
        {
            // product * quantity, order by descending
            // The popular product will be 1 and least popular will have more numbers

            _productByPopularity = _shoppingHistory
                .SelectMany(sh => sh.Products)
                .GroupBy(g => g.Name)
                .Select(groupedProduct => new { groupedProduct.Key, Total = groupedProduct.Sum(s => s.Quantity) })
                .OrderByDescending(a => a.Total)
                .Select(p => p.Key)
                .ToList<string>();
        }

        /// <summary>
        /// Return the product popularity by number
        /// </summary>
        /// <param name="productName"></param>
        /// <returns></returns>
        public int? ProductPopularity(string productName)
        {
            if (!_productByPopularity.Contains(productName))
            {
                return null;
            }

            return _productByPopularity.FindIndex(p => p.ToLower() == productName.ToLower());
        }
    }
}
