using System;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingCart.Core.Entity
{
    public class Trolley
    {
        public IEnumerable<Product> Products { get; private set; }

        public Trolley(IEnumerable<Product> products)
        {
            Products = products;

            if (Products == null)
            {
                throw new InvalidOperationException("Products is required");
            }
        }

        public decimal Total(IEnumerable<Promotion> promotions)
        {
            var actualTrolleyTotal = CaculateTrolleyTotal();
            decimal totalAfterDiscount = actualTrolleyTotal;

            if (promotions != null && promotions.Any())
            {
                foreach (var promotion in promotions)
                {
                    var discount = promotion.CalculateDiscount(this);

                    totalAfterDiscount = Math.Min(totalAfterDiscount, actualTrolleyTotal - discount);
                }
            }

            return totalAfterDiscount;
        }

        private decimal CaculateTrolleyTotal()
        {
            var total = 0.0m;
            foreach (var product in Products)
            {
                total += product.TotalPrice();
            }

            return total;
        }
    }
}