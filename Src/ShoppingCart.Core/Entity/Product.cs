using System;

namespace ShoppingCart.Core.Entity
{
    public class Product
    {
        public string Name { get; private set; }
        public decimal Price { get; private set; }
        public decimal Quantity { get; private set; }

        public int? Popularity { get; private set; }

        public Product(string name, decimal price, decimal quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }

        public void SetPopularity(PopularityCalculator popularityCalculator)
        {
            if (popularityCalculator == null)
            {
                throw new ArgumentException("Calculator is expected for calculating popularity", nameof(popularityCalculator));
            }

            this.Popularity = popularityCalculator.ProductPopularity(this.Name);
        }

        public decimal TotalPrice()
        {
            return Price * Quantity;
        }
    }
}
