using System.Collections.Generic;

namespace ShoppingCart.Core.ValueObject
{
    public class ShoppingHistoryValueObject
    {
        public long CustomerId { get; set; }

        public IEnumerable<ProductValueObject> Products { get; set; }
    }
}
