namespace ShoppingCart.Core.ValueObject
{
    public class QuantityValueObject
    {
        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Product Name
        /// </summary>
        public string Product { get; set; }
    }
}
