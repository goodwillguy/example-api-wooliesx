namespace ShoppingCart.Core.ValueObject
{
    public class ProductValueObject
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
    }
}