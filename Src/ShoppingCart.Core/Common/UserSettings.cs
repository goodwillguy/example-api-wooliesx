namespace ShoppingCart.Core.Common
{
    /// <summary>
    /// User settings
    /// </summary>
    public class UserSettings
    {
        /// <summary>
        /// Name of user
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User Token supplied by wolliesX
        /// </summary>
        public string Token { get; set; }
    }
}