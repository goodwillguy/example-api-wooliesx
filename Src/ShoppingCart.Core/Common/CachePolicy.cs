using Microsoft.Extensions.Options;
using Polly;
using Polly.CircuitBreaker;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Common
{
    public class CachePolicy : ICachePolicy
    {
        private readonly CachePolicySettings _settings;
        private readonly CircuitBreakerPolicy _policy;

        public CachePolicy(IOptions<CachePolicySettings> settings)
        {
            _settings = settings.Value;

            _policy = Policy
                .Handle<InvalidOperationException>()
                .Or<RedisConnectionException>()
                .CircuitBreakerAsync(_settings.MaxErrorCount, TimeSpan.FromMinutes(_settings.DurationOfBreakInMinutes),
                    (ex, durationOfBreak, context) => { }, context => { });
        }

        public Task<T> ExecuteAsync<T>(Func<T> func)
        {
            return _policy.ExecuteAsync(context => Task.FromResult(func()), new Dictionary<string, object> { { "methodName", func.Method.Name } });
        }

        public Task<T> ExecuteAsync<T>(Func<Task<T>> func)
        {
            return _policy.ExecuteAsync(context => func(), new Dictionary<string, object> { { "methodName", func.Method.Name } });
        }

        public Task ExecuteAsync(Func<Task> action)
        {
            return _policy.ExecuteAsync(context => action(), new Dictionary<string, object> { { "methodName", action.Method.Name } });
        }

        public bool WasCircuitBroken => _policy.CircuitState != CircuitState.Closed;

        public Exception LastException => _policy.LastException;
    }
}