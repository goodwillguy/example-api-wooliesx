using Microsoft.Extensions.Caching.Distributed;
using System;

namespace ShoppingCart.Core.Common
{
    internal class CacheExpiryHelper
    {
        /// <summary>
        /// set to 0 for infinite expiry
        /// </summary>
        /// <param name="timeoutInMinutes"></param>
        /// <returns></returns>
        public static DistributedCacheEntryOptions GetExpiration(int timeoutInMinutes)
        {
            return new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = GetExpirationTimeout(timeoutInMinutes)
            };
        }

        private static TimeSpan GetExpirationTimeout(int timeoutInMinutes)
        {
            return timeoutInMinutes == 0 ? TimeSpan.FromDays(365) : TimeSpan.FromMinutes(timeoutInMinutes);
        }
    }
}