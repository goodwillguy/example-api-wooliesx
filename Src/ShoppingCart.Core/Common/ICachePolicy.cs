using System;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Common
{
    /// <summary>
    /// Cache policy
    /// </summary>
    public interface ICachePolicy
    {
        Task<T> ExecuteAsync<T>(Func<T> func);

        Task<T> ExecuteAsync<T>(Func<Task<T>> func);

        Task ExecuteAsync(Func<Task> action);
    }
}