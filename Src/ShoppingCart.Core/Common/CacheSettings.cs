namespace ShoppingCart.Core.Common
{
    public class CacheSettings
    {
        public CacheSettings()
        {
            // Set default to 2 hours
            TimeToLiveInMins = 120;
        }

        /// <summary>
        /// Connection string from redis cache
        /// </summary>
        public string Configuration { get; set; }

        /// <summary>
        /// Settings to configure timeto live of cache
        /// </summary>
        public int TimeToLiveInMins { get; set; }
    }
}