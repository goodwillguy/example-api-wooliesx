using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ShoppingCart.Core.Interface;
using System;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Common
{
    /// <summary>
    /// Cache rapper around redis
    /// Cache is not an hard dependency, class is resilient to redis availability
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Cache<T> : ICache<T> where T : class
    {
        private IDistributedCache _distributedCache;
        private readonly ILogger<Cache<T>> _logger;
        private readonly ICachePolicy _policy;
        private readonly CacheSettings _settings;

        public Cache(ILogger<Cache<T>> logger, ICachePolicy policy, IOptions<CacheSettings> settings)
        {
            _logger = logger;
            _policy = policy;
            _settings = settings.Value;
        }

        public async Task Set(string key, T value, int timeoutInMinutes)
        {
            try
            {
                await _policy.ExecuteAsync(async () =>
                {
                    InitializeCacheIfNecessary();
                    await _distributedCache.SetStringAsync(
                        GetFormattedKey(key),
                        JsonConvert.SerializeObject(value),
                        CacheExpiryHelper.GetExpiration(timeoutInMinutes));
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not save to cache the key {GetFormattedKey(key)}");
            }
        }

        public async Task<T> Get(string key)
        {
            try
            {
                return await _policy.ExecuteAsync(async () =>
                {
                    InitializeCacheIfNecessary();
                    var cacheString = await _distributedCache.GetStringAsync(GetFormattedKey(key));

                    return cacheString == null
                        ? null
                        : JsonConvert.DeserializeObject<T>(cacheString);
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not retrieve from cache the key {GetFormattedKey(key)}");
                return default(T);
            }
        }

        public async Task<bool> Exists(string key)
        {
            try
            {
                return await _policy.ExecuteAsync(async () =>
                {
                    InitializeCacheIfNecessary();
                    return !string.IsNullOrWhiteSpace(await _distributedCache.GetStringAsync(GetFormattedKey(key)));
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not check exists from cache the key {GetFormattedKey(key)}");
                return false;
            }
        }

        public async Task<bool> Delete(string key)
        {
            try
            {
                return await _policy.ExecuteAsync(async () =>
                {
                    InitializeCacheIfNecessary();
                    await _distributedCache.RemoveAsync(GetFormattedKey(key));
                    return true;
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not delete from cache the key {GetFormattedKey(key)}");
                return false;
            }
        }

        private string GetFormattedKey(string key)
        {
            return key.ToLower();
        }

        private void InitializeCacheIfNecessary()
        {
            if (_distributedCache != null)
            {
                return;
            }

            try
            {
                _distributedCache = new RedisCache(new RedisCacheOptions
                {
                    Configuration = _settings.Configuration
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Cannot initialize cache");
                throw;
            }
        }

        public async Task<T> GetOrAdd(string key, Func<string, Task<T>> valueFactory, int defaultTimeout = 1)
        {
            var valueInCache = await Get(key);

            if (valueInCache != null)
            {
                return valueInCache;
            }

            var value = await valueFactory.Invoke(key);

            await Set(key, value, defaultTimeout);

            return value;
        }
    }
}