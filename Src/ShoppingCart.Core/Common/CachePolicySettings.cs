namespace ShoppingCart.Core.Common
{
    public class CachePolicySettings
    {
        public int MaxErrorCount { get; set; }
        public int DurationOfBreakInMinutes { get; set; }
    }
}