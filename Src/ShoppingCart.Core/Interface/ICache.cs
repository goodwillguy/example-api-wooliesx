using System;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Interface
{
    /// <summary>
    /// Caching abstraction interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICache<T> where T : class
    {
        Task<T> Get(string key);

        Task<bool> Delete(string key);

        Task Set(string key, T value, int timeoutInMinutes);

        Task<bool> Exists(string key);

        Task<T> GetOrAdd(string key, Func<string, Task<T>> valueFactory, int defaultTimeout = 1);
    }
}