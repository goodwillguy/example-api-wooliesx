using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Interface
{
    public interface IProductService
    {
        Task<IEnumerable<Entity.Product>> GetAllProducts();
    }
}