using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingCart.Core.Interface
{
    public interface IShoppingHistoryService
    {
        Task<IEnumerable<ShoppingHistoryValueObject>> GetAllShoppingHistory();
    }
}