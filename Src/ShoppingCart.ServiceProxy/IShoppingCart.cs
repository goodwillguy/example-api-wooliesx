using Refit;
using ShoppingCart.Api.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingCart.ServiceProxy
{
    /// <summary>
    /// Endpoints to call Shopping Cart microservice
    /// </summary>
    public interface IShoppingCart
    {
        /// <summary>
        /// Get WolliesX user token and name information
        /// </summary>
        /// <returns></returns>
        [Get("/user")]
        Task<UserResponse> GetUserInformation();

        [Get("/sort")]
        Task<IEnumerable<ProductResponse>> GetSortedProducts(string sortOption);
    }
}
