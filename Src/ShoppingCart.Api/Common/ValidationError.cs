namespace ShoppingCart.Api.Common
{
    internal class ValidationError
    {
        public string Message { get; set; }
    }
}