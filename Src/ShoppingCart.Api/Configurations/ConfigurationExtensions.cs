using ShoppingCart.Core.Configuration;
using ShoppingCart.Infrastructure.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ShoppingCart.Api.Configurations
{
    internal static class ConfigurationExtensions
    {
        /// <summary>
        /// Configure service dependencies
        /// </summary>
        /// <param name="services">service collection</param>
        /// <param name="configuration">configuration</param>
        /// <returns>mediatr services</returns>
        internal static IServiceCollection AddServiceConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                    .AddCoreServiceConfigurations(configuration)
                    .AddInfrastructureConfigurations(configuration);
        }
    }
}
