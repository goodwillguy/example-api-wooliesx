namespace ShoppingCart.Api.Configurations
{
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using ShoppingCart.Api.Filters;

    /// <summary>
    /// Setup MVC Dependency injection
    /// </summary>
    public static class MvcExtensions
    {
        /// <summary>
        /// MVC dependency injection
        /// The json serialisation need to be camel case
        /// </summary>
        /// <param name="services">service collection</param>
        /// <returns>registered mvc filters and json serialization</returns>
        public static IServiceCollection AddMvcAndConfigure(this IServiceCollection services)
        {
            services
                .AddMvc(options =>
                {
                    options.Filters.Add(new ExceptionFilter());
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Formatting = Formatting.Indented;
                });

            return services;
        }
    }
}