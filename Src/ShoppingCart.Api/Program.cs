using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using ShoppingCart.Api.Logging;
using System;

namespace ShoppingCart.Api
{
    internal class Program
    {
        /// <summary>
        /// Application function entry
        /// </summary>
        /// <param name="args">command line arguments</param>
        public static void Main(string[] args)
        {
            try
            {
                IWebHost webHost = BuildWebHost(args);
                Log.ForContext<Program>().Information("Shopping Cart Api is starting");
                webHost.Run();
            }
            catch (Exception ex)
            {
                Log.ForContext<Program>().Fatal(ex, "Shopping Cart Api terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        /// <summary>
        /// Build webhost for the web application
        /// </summary>
        /// <param name="args">command line arguments</param>
        /// <returns>webhost</returns>
        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
                .UseKestrel(option => option.AllowSynchronousIO = false)
                .UseIIS()
                .UseStartup<Startup>()
                .UseSerilog()
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    var env = builderContext.HostingEnvironment;
                    config
                        .SetBasePath(env.ContentRootPath + "/Settings/")
                        .AddJsonFile("appsettings.json", false, true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                        .AddJsonFile($"{env.EnvironmentName}/appsettings.json", true)
                        .AddEnvironmentVariables()
                        .AddLoggingConfiguration(typeof(Program).Assembly.GetName().Name);
                })
                .Build();
    }
}