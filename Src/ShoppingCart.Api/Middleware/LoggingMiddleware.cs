using ShoppingCart.Api.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart.Api.Middleware
{
    /// <summary>
    /// Captures and logs (info) the request URL and also the start/end time of a request. This middleware must be the first invocation.
    /// </summary>
    internal sealed class LoggingMiddleware
    {
        private const string CorrelationIdHeader = "X-Correlation-Id";
        private const string LoadBalancerTraceIdHeader = "X-Amzn-Trace-Id";

        private readonly ILogger<LoggingMiddleware> _logger;
        private readonly RequestDelegate _next;

        public LoggingMiddleware(
            ILogger<LoggingMiddleware> logger,
            RequestDelegate next)
        {
            _logger = logger;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

            using (ContextCorrelator.Instance
                        .AddProperty("TraceId", GetLoadBalancerTraceId(context))
                        .AddProperty("CorrelationId", GetCorrelationIdHeader(context))
                        .BeginCorrelationScope(_logger))
            {
                var requestURL = context.Request.GetDisplayUrl();
                _logger.LogInformation("Start processing for Request: {@requestURL}", requestURL);

                try
                {
                    await _next(context);
                }
                finally
                {
                    sw.Stop();

                    _logger.LogInformation("Finished processing for Request: {@requestURL}. StatusCode: {@statusCode}. Took: {@elapsed} ms.",
                            requestURL,
                            context.Response?.StatusCode,
                            sw.ElapsedMilliseconds);
                }
            }
        }

        private string GetCorrelationIdHeader(HttpContext context)
        {
            if (context.Request?.Headers == null)
            {
                return Guid.NewGuid().ToString();
            }

            // get the actual correlation id passed from the upstream service
            if (context.Request.Headers.TryGetValue(CorrelationIdHeader, out StringValues correlationIds))
            {
                return correlationIds.First();
            }

            // use the request id
            if (!string.IsNullOrEmpty(context.TraceIdentifier))
            {
                return context.TraceIdentifier;
            }

            // or construct the correlation id from guid
            return Guid.NewGuid().ToString();
        }

        private string GetLoadBalancerTraceId(HttpContext context)
        {
            if (context.Request.Headers.TryGetValue(LoadBalancerTraceIdHeader, out StringValues traceId))
            {
                return traceId.First();
            }

            return string.Empty;
        }
    }
}
