namespace ShoppingCart.Api.Filters
{
    using FluentValidation;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.Extensions.Logging;
    using ShoppingCart.Api.Common;
    using System.Net;

    /// <summary>
    /// Global controller exception filter
    /// </summary>
    public class ExceptionFilter : IExceptionFilter
    {
        /// <summary>
        /// Log exception if not handled. A handled exception is where you have a response object in result
        /// </summary>
        /// <param name="context">exception context</param>
        public void OnException(ExceptionContext context)
        {
            // is exceptionhandled
            if (context.Result != null)
            {
                return;
            }

            var exception = context.Exception;
            object errorContent = null;
            HttpStatusCode statusCode;

            var logger = (ILogger<ExceptionFilter>)context.HttpContext.RequestServices.GetService(typeof(ILogger<ExceptionFilter>));

            switch (exception)
            {
                case ValidationException _:
                    // Todo: Add more description to validation error
                    errorContent = new ValidationError { Message = context.Exception.Message };
                    statusCode = HttpStatusCode.BadRequest;
                    logger.LogWarning(exception, exception.Message);
                    break;

                default:
                    errorContent = new { Message = context.Exception.Message };
                    statusCode = HttpStatusCode.InternalServerError;
                    logger.LogError(context.Exception, context.Exception.Message);
                    break;
            }

            context.Result = new ObjectResult(errorContent)
            {
                StatusCode = (int)statusCode
            };
        }
    }
}