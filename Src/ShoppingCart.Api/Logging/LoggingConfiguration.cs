namespace ShoppingCart.Api.Logging
{
    using ShoppingCart.Api.Logging.Enrichers;
    using Microsoft.Extensions.Configuration;
    using Serilog;

    public static class LoggingConfiguration
    {
        public static void AddLoggingConfiguration(this IConfigurationBuilder configurationBuilder, string applicationName)
        {
            var config = configurationBuilder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .Enrich.WithProperty("ApplicationName", applicationName)
                .Enrich.With<UtcTimestampEnricher>()
                .Enrich.With<ExceptionOutputEnricher>()
                .Enrich.With<RuntimeVersionEnricher>()
                .Enrich.FromLogContext()
                .CreateLogger();
        }
    }
}