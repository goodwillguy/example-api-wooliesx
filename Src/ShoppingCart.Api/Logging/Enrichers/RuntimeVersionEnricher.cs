namespace ShoppingCart.Api.Logging.Enrichers
{
    using Serilog.Core;
    using Serilog.Events;
    using System;
    using System.Reflection;

    public class RuntimeVersionEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent == null)
            {
                throw new ArgumentNullException("logEvent");
            }

            var version = Assembly.GetEntryAssembly().GetName().Version.ToString();

            if (version == null)
            {
                return;
            }

            var applicationName = new LogEventProperty("RuntimeVersion", new ScalarValue(version));
            logEvent.AddPropertyIfAbsent(applicationName);
        }
    }
}