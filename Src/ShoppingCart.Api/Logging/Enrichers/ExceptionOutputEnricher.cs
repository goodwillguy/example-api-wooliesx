namespace ShoppingCart.Api.Logging.Enrichers
{
    using Serilog.Core;
    using Serilog.Events;
    using System;

    /// <summary>
    /// Remove new line from exception string.
    /// </summary>
    public class ExceptionOutputEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent.Exception == null)
            {
                return;
            }

            var logEventProperty = propertyFactory.CreateProperty("EscapedException", logEvent.Exception.ToString().Replace(Environment.NewLine, " \\r "));

            logEvent.AddPropertyIfAbsent(logEventProperty);
        }
    }
}