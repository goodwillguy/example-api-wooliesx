namespace ShoppingCart.Api.Logging
{
    using System;
    using System.Collections.Immutable;
    using System.Threading;

    public class ContextCorrelator
    {
        private static AsyncLocal<ImmutableDictionary<string, object>> _items = new AsyncLocal<ImmutableDictionary<string, object>>();

        private static ImmutableDictionary<string, object> Items
        {
            get => _items.Value == null ? _items.Value = ImmutableDictionary<string, object>.Empty : _items.Value;
            set => _items.Value = value;
        }

        private static ContextCorrelator _instance;

        public static ContextCorrelator Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ContextCorrelator();
                }
                return _instance;
            }
        }

        private ContextCorrelator()
        {
        }

        public object GetValue(string key) => Items[key];

        public bool TryGetValue(string key, out object value)
        {
            return Items.TryGetValue(key, out value);
        }

        public ContextCorrelator AddProperty(string key, string value)
        {
            if (Items.ContainsKey(key))
            {
                throw new InvalidOperationException($"{key} is already being correlated!");
            }

            Items = Items.Add(key, value);
            return this;
        }

        public IDisposable BeginCorrelationScope(Microsoft.Extensions.Logging.ILogger logger)
        {
            return new CorrelationScope(logger.BeginScope(Items));
        }

        internal class CorrelationScope : IDisposable
        {
            private readonly IDisposable _logContextPop;

            public CorrelationScope(IDisposable logContextPop)
            {
                _logContextPop = logContextPop ?? throw new ArgumentNullException(nameof(logContextPop));
            }

            public void Dispose() => _logContextPop.Dispose();
        }
    }
}