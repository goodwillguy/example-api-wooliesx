namespace ShoppingCart.Api
{
    using ShoppingCart.Api.Configurations;
    using ShoppingCart.Api.Middleware;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Serilog;

    /// <summary>
    /// Startup class used for web configuration
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Setting configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">configuration</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configure the app with mvc, exception handler etc.
        /// </summary>
        /// <param name="app">app builder</param>
        /// <param name="env">hosting environment</param>
        /// <param name="appLifetime">app lifetime</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // The health middleware should be first, since it needs to be fast and need not do logging..
            // .net core calls middleware in sequence of registration
            app.UseHealthChecks("/diagnostics/ping");

            // Configure to use correlation id and traces
            app.UseMiddleware<LoggingMiddleware>();

            app.UseMvc();

            appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);
        }

        /// <summary>
        /// Setting up dependency injection for the applications
        /// </summary>
        /// <param name="services">service collection</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvcAndConfigure()
                .AddServiceConfigurations(Configuration)
                .AddHealthChecks();
        }
    }
}