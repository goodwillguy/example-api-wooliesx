namespace ShoppingCart.Api.Controller
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using ShoppingCart.Api.Contracts;
    using ShoppingCart.Core.User.Get;
    using System.Threading.Tasks;

    /// <summary>
    /// Get user information
    /// </summary>
    [Route("user")]
    public class UserController : Controller
    {
        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get User Information
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(UserResponse), 200)]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new UserRequest());
            return Ok(response);
        }
    }
}
