namespace ShoppingCart.Api.Controller
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using ShoppingCart.Api.Contracts;
    using ShoppingCart.Core.Application.Product.Sort;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Sort product controller
    /// </summary>
    [Route("sort")]
    public class SortController : Controller
    {
        private readonly IMediator _mediator;

        public SortController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [ProducesResponseType(typeof(IEnumerable<ProductResponse>), 200)]
        public async Task<IActionResult> SortProducts([FromQuery] SortRequestQuery sortRequest)
        {
            var response = await _mediator.Send(sortRequest);
            return Ok(response);
        }

    }
}
