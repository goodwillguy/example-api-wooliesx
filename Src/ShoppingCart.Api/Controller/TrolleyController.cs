
namespace ShoppingCart.Api.Controller
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using ShoppingCart.Core.Application.Trolley;
    using System.Threading.Tasks;

    [Route("trolleyTotal")]
    public class TrolleyController : Controller
    {
        private readonly IMediator _mediator;

        public TrolleyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> CalculateTotal([FromBody] TrolleyCommand trolleyCommand)
        {
            var response = await _mediator.Send(trolleyCommand);
            return Ok(response);
        }
    }
}
