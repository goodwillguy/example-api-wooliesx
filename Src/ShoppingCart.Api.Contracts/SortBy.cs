namespace ShoppingCart.Api.Contracts
{
    public enum SortBy
    {
        /// <summary>
        /// Low To High Price
        /// </summary>
        Low = 0,

        /// <summary>
        /// High to Low Price
        /// </summary>
        High,

        /// <summary>
        /// Ascending by Name
        /// </summary>
        Ascending,

        /// <summary>
        /// Descending by Name
        /// </summary>
        Descending,

        /// <summary>
        /// Recommended by shopping history
        /// </summary>
        Recommended
    }
}