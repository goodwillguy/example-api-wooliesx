namespace ShoppingCart.Api.Contracts
{
    public class UserResponse
    {
        public string Name { get; set; }
        public string Token { get; set; }
    }
}