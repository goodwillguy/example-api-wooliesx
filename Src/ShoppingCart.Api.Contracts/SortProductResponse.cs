namespace ShoppingCart.Api.Contracts
{
    public class ProductResponse
    {
        /// <summary>
        /// product Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Product Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Product Quantity
        /// </summary>
        public decimal Quantity { get; set; }
    }
}