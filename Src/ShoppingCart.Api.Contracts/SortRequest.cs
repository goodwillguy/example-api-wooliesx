using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ShoppingCart.Api.Contracts
{
    public class SortRequest
    {
        /// <summary>
        /// Sort by option
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public SortBy SortOption { get; set; }
    }
}