namespace ShoppingCart.Api.Contracts
{
    public class ProductData
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}