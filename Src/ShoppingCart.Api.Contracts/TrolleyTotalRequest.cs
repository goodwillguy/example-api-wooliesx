using System.Collections.Generic;

namespace ShoppingCart.Api.Contracts
{
    public class TrolleyTotalRequest
    {
        public IEnumerable<ProductData> Products { get; set; }
        public IEnumerable<Specials> Specials { get; set; }
        public IEnumerable<ProductQuantity> Quantities { get; set; }
    }
}