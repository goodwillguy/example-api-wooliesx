using System.Collections.Generic;

namespace ShoppingCart.Api.Contracts
{
    public class Specials
    {
        public IEnumerable<ProductQuantity> Quantities { get; set; }
        public decimal Total { get; set; }
    }
}