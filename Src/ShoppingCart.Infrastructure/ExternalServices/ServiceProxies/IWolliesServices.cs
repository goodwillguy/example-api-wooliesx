using Refit;
using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingCart.Infrastructure.ExternalServices.ServiceProxies
{
    internal interface IWolliesServices
    {
        /// <summary>
        /// Get all products from wolliesX
        /// </summary>
        /// <param name="token">api token</param>
        /// <returns></returns>
        [Get("/api/resource/products?token={token}")]
        Task<IEnumerable<ProductDto>> GetAllProducts(string token);

        /// <summary>
        /// Get all customer shopping history
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [Get("/api/resource/shopperHistory?token={token}")]
        Task<IEnumerable<ShoppingHistoryValueObject>> GetAllShoppingHistory(string token);
    }
}
