using Microsoft.Extensions.Options;
using ShoppingCart.Core.Common;
using ShoppingCart.Core.Interface;
using ShoppingCart.Core.ValueObject;
using ShoppingCart.Infrastructure.ExternalServices.ServiceProxies;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingCart.Infrastructure.ExternalServices
{
    internal class ShoppingHistoryService : IShoppingHistoryService
    {
        private readonly IWolliesServices _wolliesServices;
        private readonly UserSettings _userSettings;

        public ShoppingHistoryService(IOptions<UserSettings> userOptions, IWolliesServices wolliesServices)
        {
            _wolliesServices = wolliesServices;
            _userSettings = userOptions.Value;
        }

        public async Task<IEnumerable<ShoppingHistoryValueObject>> GetAllShoppingHistory()
        {
            return await _wolliesServices.GetAllShoppingHistory(_userSettings.Token);
        }
    }
}