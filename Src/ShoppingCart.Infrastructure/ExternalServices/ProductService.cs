using Microsoft.Extensions.Options;
using ShoppingCart.Core.Common;
using ShoppingCart.Core.Entity;
using ShoppingCart.Core.Interface;
using ShoppingCart.Infrastructure.ExternalServices.ServiceProxies;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart.Infrastructure.ExternalServices
{
    internal class ProductService : IProductService
    {
        private readonly IWolliesServices _wolliesServices;
        private readonly UserSettings _userSettings;

        public ProductService(IOptions<UserSettings> userOptions, IWolliesServices wolliesServices)
        {
            _wolliesServices = wolliesServices;
            _userSettings = userOptions.Value;
        }

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            var response = await _wolliesServices.GetAllProducts(_userSettings.Token);

            if (response == null)
            {
                return new List<Product>();
            }

            return response.Select(p => new Product(p.Name, p.Price, p.Quantity));
        }
    }
}