using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Refit;
using ShoppingCart.Core.Interface;
using ShoppingCart.Infrastructure.Common;
using ShoppingCart.Infrastructure.ExternalServices;
using ShoppingCart.Infrastructure.ExternalServices.ServiceProxies;

namespace ShoppingCart.Infrastructure.Configuration
{
    public static class ConfigurationExtenstion
    {
        /// <summary>
        /// Configure service dependencies
        /// </summary>
        /// <param name="services">service collection</param>
        /// <param name="configuration">configuration</param>
        /// <returns>mediatr services</returns>
        public static IServiceCollection AddInfrastructureConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services
             .AddSingleton(typeof(IWolliesServices), (provider) =>
              {
                  var wooliesSettings = provider.GetService<IOptions<WolliesSettings>>();

                  return RestService.For<IWolliesServices>(wooliesSettings.Value.BaseUrl);
              });

            return services
                    .AddScoped<IProductService, ProductService>()
                    .Configure<WolliesSettings>(configuration.GetSection("WolliesX"))
                    .AddScoped<IShoppingHistoryService, ShoppingHistoryService>();
        }
    }
}
