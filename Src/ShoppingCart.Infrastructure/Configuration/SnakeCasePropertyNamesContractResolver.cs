namespace ShoppingCart.Infrastructure.Configuration
{
    using Newtonsoft.Json.Serialization;

    public class SnakeCasePropertyNamesContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            for (int i = propertyName.Length - 1; i > 1; i--)
            {
                if (char.IsUpper(propertyName[i]))
                {
                    propertyName = propertyName.Insert(i, "_");
                }
            }

            return propertyName.ToLower();
        }
    }
}