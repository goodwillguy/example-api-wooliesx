# README #

This is example api. It is used to get products, sort products and get trolly total

## Running the service/ Deployment strategy

The service will deployed on Azure.

To run on your local do the run the following command

./Build/build.sh
./Build/runApi.sh

## Technologies used

The application is written in .net core 2.2. 

## Usage in application

This project exposes the endpoints type-safe REST interface and contracts via nuget.

You can add the nuget from the location TO BE DECIDED.

Currently, the services contract exposed is only a .NET Framework client. The service uses a library called Refit to expose the contracts. 

The details can be found 

https://github.com/reactiveui/refit

## End Point definition

The endpoint definition could be collected from swagger.

The endpoing is TO BE ADDED




* Repo owner or admin
* Other community or team contact