using FluentAssertions;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ShoppingCart.Api.AcceptanceTest.Steps.User
{
    public class UserStep : StepBase
    {
        public UserStep(FeatureContext featureContext, ScenarioContext scenarioContext) : base(featureContext, scenarioContext)
        {
        }

        [Given(@"An a shopping cart service")]
        public void GivenAnAShoppingCartService()
        {
        }

        [Then(@"I Should receive (.*) status")]
        public async Task ThenIShouldReceiveStatus(int p0)
        {
            var response = await TestContext.ShoppingCartApi.GetUserInformation();

            response.Name.Should().NotBeNullOrEmpty();
            response.Token.Should().NotBeNullOrEmpty();
        }

    }
}
