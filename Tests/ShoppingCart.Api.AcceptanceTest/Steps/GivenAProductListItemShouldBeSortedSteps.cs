using FluentAssertions;
using ShoppingCart.Api.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ShoppingCart.Api.AcceptanceTest.Steps
{
    [Binding]
    public class GivenAProductListItemShouldBeSortedSteps : StepBase
    {
        public GivenAProductListItemShouldBeSortedSteps(FeatureContext featureContext, ScenarioContext scenarioContext) : base(featureContext, scenarioContext)
        {
        }

        [Given(@"Sort option (.*)")]
        public async Task GivenSortOption(string sortOption)
        {
            //TODO: Extend for other sort options. 
            var response = await TestContext.ShoppingCartApi.GetSortedProducts(sortOption);

            StepFeatureContext.Add("AscendingResponse", response);

        }

        [Then(@"the result should be (.*) with product sorted")]
        public void ThenTheResultShouldBeWithProductSorted(int p0)
        {
            var response = StepFeatureContext.Get<IEnumerable<ProductResponse>>("AscendingResponse");

            var expectedResponse = TestMockData.ProductCollection.OrderBy(p => p.Name).Select(p => new ProductResponse { Name = p.Name, Price = p.Price, Quantity = p.Quantity });

            response.Should().BeEquivalentTo(expectedResponse);
        }
    }
}
