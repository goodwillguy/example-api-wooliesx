using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Moq.AutoMock;
using Refit;
using ShoppingCart.Api.AcceptanceTest.Setup;
using ShoppingCart.Core.ValueObject;
using ShoppingCart.Infrastructure.ExternalServices.ServiceProxies;
using ShoppingCart.ServiceProxy;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ShoppingCart.Api.AcceptanceTest.Steps
{
    internal static class TestContext
    {
        internal static IWebHost WebHost { get; set; }

        internal static IShoppingCart ShoppingCartApi { get; set; }

        internal static IWolliesServices WolliesServices { get; set; }

        //internal static FluentMockServerWrapper WolliesServices { get; set; }
    }

    internal static class TestMockData
    {
        internal static IEnumerable<ProductDto> ProductCollection
        {
            get
            {
                return new List<ProductDto>
                {
                    new ProductDto{Name="Test Product A",Price=99.99m,Quantity=0},
                    new ProductDto{Name="Test Product B",Price=101.99m,Quantity=0},
                    new ProductDto{Name="Test Product C",Price=10.99m,Quantity=0},
                    new ProductDto{Name="Test Product D",Price=5.0m,Quantity=0},
                    new ProductDto{Name="Test Product F",Price=999999999999.0m,Quantity=0}
                };
            }
        }
        internal static IEnumerable<ShoppingHistoryValueObject> ShoppingHistories
        {
            get
            {
                return new List<ShoppingHistoryValueObject>
                {
                    new ShoppingHistoryValueObject
                    {
                        CustomerId=123,
                        Products=new List<ProductValueObject>
                        {
                            new ProductValueObject{Name="Test Product A",Price=99.99m,Quantity=3.0m},
                            new ProductValueObject{Name="Test Product B",Price=101.99m,Quantity=1.0m},
                            new ProductValueObject{Name="Test Product F",Price=999999999999.0m,Quantity=1.0m}
                        }
                    },
                    new ShoppingHistoryValueObject
                    {
                        CustomerId=23,
                        Products=new List<ProductValueObject>
                        {
                            new ProductValueObject{Name="Test Product A",Price=99.99m,Quantity=2.0m},
                            new ProductValueObject{Name="Test Product B",Price=101.99m,Quantity=3.0m},
                            new ProductValueObject{Name="Test Product F",Price=999999999999.0m,Quantity=1.0m}
                        }
                    },
                    new ShoppingHistoryValueObject
                    {
                        CustomerId=23,
                        Products=new List<ProductValueObject>
                        {
                            new ProductValueObject{Name="Test Product C",Price=10.99m,Quantity=2.0m},
                            new ProductValueObject{Name="Test Product F",Price=999999999999.0m,Quantity=2.0m}
                        }
                    },
                    new ShoppingHistoryValueObject
                    {
                        CustomerId=23,
                        Products=new List<ProductValueObject>
                        {
                             new ProductValueObject{Name="Test Product A",Price=99.99m,Quantity=1.0m},
                            new ProductValueObject{Name="Test Product B",Price=101.99m,Quantity=1.0m},
                            new ProductValueObject{Name="Test Product C",Price=10.99m,Quantity=1.0m}
                        }
                    }
                };
            }
        }
    }

    [Binding]
    public abstract class StepBase
    {
        public FeatureContext StepFeatureContext { get; }
        public ScenarioContext StepScenarioContext { get; }
        public StepBase(FeatureContext featureContext, ScenarioContext scenarioContext)
        {
            StepFeatureContext = featureContext;
            StepScenarioContext = scenarioContext;
        }

        [BeforeTestRun]
        protected static async Task OneTimeSetup()
        {
            // Setup Wollies

            MockWolliesServices();

            // Setup the Api to test
            SetupAndStartExchangeRateApi();
        }

        private static void MockWolliesServices()
        {
            // ToDo: Remove interface mock and setup as http mock

            AutoMocker autoMocker = new AutoMocker();
            TestContext.WolliesServices = autoMocker.GetMock<IWolliesServices>().Object;
            autoMocker
                .GetMock<IWolliesServices>()
                .Setup(mo => mo.GetAllProducts(It.IsAny<string>()))
                .ReturnsAsync(TestMockData.ProductCollection);

            autoMocker
               .GetMock<IWolliesServices>()
               .Setup(mo => mo.GetAllShoppingHistory(It.IsAny<string>()))
               .ReturnsAsync(TestMockData.ShoppingHistories);
        }

        private static void SetupAndStartExchangeRateApi()
        {
            try
            {
                var builder = HostFactory.BuildWeb();

                builder.ConfigureTestServices(x =>
                {
                    x.AddSingleton(y => TestContext.WolliesServices);
                });

                var testServer = new TestServer(builder);
                var client = testServer.CreateClient();

                // Set the client to be used throughout the test
                TestContext.ShoppingCartApi = RestService.For<IShoppingCart>(client);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
