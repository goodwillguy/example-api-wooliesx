using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ShoppingCart.Api.AcceptanceTest.Setup
{
    public class HostFactory
    {
        public static IWebHostBuilder BuildWeb()
        {
            var webHostBuilder = WebHost
                .CreateDefaultBuilder()
                .UseKestrel()
                .UseEnvironment("Test")
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    var env = builderContext.HostingEnvironment;
                    config
                        .SetBasePath(env.ContentRootPath + "/Settings/")
                        .AddJsonFile("appsettings.json", false, true)
                        .AddJsonFile("appsettings.Test.json", false, true)
                        .AddEnvironmentVariables();
                })
                .UseStartup<Startup>();

            return webHostBuilder;
        }
    }
}
