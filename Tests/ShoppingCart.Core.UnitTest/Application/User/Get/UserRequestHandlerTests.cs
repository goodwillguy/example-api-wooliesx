using FluentAssertions;
using Microsoft.Extensions.Options;
using Moq.AutoMock;
using ShoppingCart.Api.Contracts;
using ShoppingCart.Core.Common;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ShoppingCart.Core.Application.User.Get.Tests
{
    public class UserRequestHandlerTests
    {
        private AutoMocker Mocker = new AutoMocker();

        internal UserRequestHandler Target { get; private set; }

        [Fact()]
        public async Task Should_Throw_Error_On_Token_Null()
        {
            // Arrange

            Mocker.Use(Options.Create(new UserSettings()));
            var target = Mocker.CreateInstance<UserRequestHandler>();

            // Act

            Func<Task> func = () => target.Handle(new Core.User.Get.UserRequest(), default);

            // Assert

            await Assert.ThrowsAsync<InvalidOperationException>(func);
        }

        [Fact()]
        public async Task Should_Respond_With_User_Information()
        {
            // Arrange

            Mocker.Use(Options.Create(new UserSettings { Name = "dsk", Token = "123" }));
            var target = Mocker.CreateInstance<UserRequestHandler>();

            // Act

            var response = await target.Handle(new Core.User.Get.UserRequest(), default);

            // Assert

            UserResponse expectedResult = new UserResponse { Name = "dsk", Token = "123" };
            response.Should().BeEquivalentTo(expectedResult);
        }
    }
}