using FluentAssertions;
using Moq.AutoMock;
using ShoppingCart.Api.Contracts;
using ShoppingCart.Core.Entity.Sorter;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ShoppingCart.Core.Application.Product.Sort.Tests
{
    public class ProductSorterFactoryTests
    {
        private AutoMocker _autoMocker = new AutoMocker();

        [Theory]
        [InlineData(SortBy.Ascending, typeof(NameSorter))]
        [InlineData(SortBy.Descending, typeof(NameSorter))]
        [InlineData(SortBy.Low, typeof(PriceSorter))]
        [InlineData(SortBy.High, typeof(PriceSorter))]
        public async Task Should_Return_Sorter(SortBy sortBy, Type expectedObject)
        {
            // Arrange

            var target = _autoMocker.CreateInstance<ProductSorterFactory>();

            // Act

            var result = await target.GetProductSorter(null, new SortRequestQuery { SortOption = sortBy });

            // Assert

            result.Should().BeOfType(expectedObject);
        }

        [Fact()]
        public async Task Should_Return_Recommended_Sorter()
        {
            // Arrange

            var target = _autoMocker.CreateInstance<ProductSorterFactory>();

            // Act

            var result = await target.GetProductSorter(new List<Entity.Product>(), new SortRequestQuery { SortOption = SortBy.Recommended });

            // Assert

            result.Should().BeOfType(typeof(RecommendedSorter));
        }
    }
}