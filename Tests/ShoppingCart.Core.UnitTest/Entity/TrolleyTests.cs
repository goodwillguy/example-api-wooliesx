using FluentAssertions;
using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using Xunit;

namespace ShoppingCart.Core.Entity.Tests
{
    public class TrolleyTests
    {
        [Fact()]
        public void Should_Return_TrolleyTotal_Without_Discounts_Price()
        {
            // Arrange

            var trolley = new Trolley(new List<Product> { new Product("1", 1, 8), new Product("2", 1, 3) });

            // Act

            var total = trolley.Total(null);

            // Assert

            total.Should().Be(11);
        }

        [Fact()]
        public void Should_Return_TrolleyTotal_With_Discounts()
        {
            // Arrange

            var trolley = new Trolley(new List<Product> { new Product("1", 5, 8), new Product("2", 1, 3) });

            // Act

            var total = trolley.Total(new List<Promotion>
                                    {
                                        new Promotion(new List<QuantityValueObject>
                                        {
                                            new QuantityValueObject { Product = "1", Quantity = 2 }
                                        },2)
                                    });

            // Assert

            total.Should().Be(11);
        }

        [Fact()]
        public void Should_Return_TrolleyTotal_With_Discounts_With_Multiple_Promotions()
        {
            // Arrange

            var trolley = new Trolley(new List<Product> { new Product("1", 5, 8), new Product("2", 1, 3) });

            // Act

            var total = trolley.Total(new List<Promotion>
                                    {
                                        new Promotion(new List<QuantityValueObject>
                                        {
                                            new QuantityValueObject { Product = "1", Quantity = 2 }
                                        },2),
                                         new Promotion(new List<QuantityValueObject>
                                        {
                                            new QuantityValueObject { Product = "1", Quantity = 2 }
                                        },1)
                                    });

            // Assert

            total.Should().Be(7);
        }
    }
}