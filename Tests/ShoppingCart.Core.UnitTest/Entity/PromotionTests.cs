using FluentAssertions;
using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using Xunit;

namespace ShoppingCart.Core.Entity.Tests
{
    public class PromotionTests
    {
        [Fact()]
        public void Should_Return_0()
        {
            // Arrange

            var promotion = new Promotion(null, 0);

            // Act

            var result = promotion.CalculateDiscount(null);

            // Assert
            result.Should().Be(0);
        }

        [Fact()]
        public void Should_Return_Discount()
        {
            // Arrange  

            var promotion = new Promotion(new List<QuantityValueObject> { new QuantityValueObject { Product = "1", Quantity = 2 } }, 1);

            // Act

            var discount = promotion.CalculateDiscount(new Trolley
            (
               new List<Product> { new Product("1", 1, 8) }
            ));

            // Assert

            discount.Should().Be(4);
        }

        [Fact()]
        public void Should_Return_Discount_Ommit_NoDiscount()
        {
            // Arrange  

            var promotion = new Promotion(new List<QuantityValueObject> { new QuantityValueObject { Product = "1", Quantity = 2 } }, 1);

            // Act

            var discount = promotion.CalculateDiscount(new Trolley
            (
               new List<Product> { new Product("1", 1, 9) }
            ));

            // Assert

            discount.Should().Be(4);
        }

        [Fact()]
        public void Should_Return_Discount_Ommit_NoDiscount_MultipleQuantity()
        {
            // Arrange  

            var promotion = new Promotion(new List<QuantityValueObject> { new QuantityValueObject { Product = "1", Quantity = 2 }, new QuantityValueObject { Product = "2", Quantity = 4 } }, 1);

            // Act

            var discount = promotion.CalculateDiscount(new Trolley
            (
               new List<Product> { new Product("1", 1, 8), new Product("2", 1, 3) }
            ));

            // Assert

            discount.Should().Be(0);
        }

        [Fact()]
        public void Should_Return_Discount_Ommit_NoDiscount_MultipleQuantity_SelectMinimum_OneBatch()
        {
            // Arrange  

            var promotion = new Promotion(new List<QuantityValueObject>
                                                {   new QuantityValueObject { Product = "1", Quantity = 2 },
                                                    new QuantityValueObject { Product = "2", Quantity = 4 } }
                                            , 2);

            // Act

            var discount = promotion.CalculateDiscount(new Trolley
            (
               new List<Product> { new Product("1", 10, 8), new Product("2", 20, 4) }
            ));

            // Assert

            discount.Should().Be(98);
        }
    }
}