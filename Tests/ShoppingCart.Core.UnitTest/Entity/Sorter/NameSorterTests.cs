using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace ShoppingCart.Core.Entity.Sorter.Tests
{
    public class NameSorterTests
    {
        private readonly List<Product> _products;

        public NameSorterTests()
        {
            _products = new List<Product>
            {
                new Product("a",2,1),
                new Product("z",2,1),
                new Product("s",2,1)
            };
        }

        [Fact()]
        public void Should_Return_Empty_List()
        {
            // Arrange

            var target = new NameSorter(null, SortDirection.Ascending);

            // Act

            var result = target.Sort();

            // Assert

            result.Should().NotBeNull();
            result.Should().HaveCount(0);
        }

        [Fact()]
        public void Should_Sort_Product_By_Ascending()
        {
            // Arrange

            var target = new NameSorter(_products, SortDirection.Ascending);

            // Act

            var result = target.Sort();

            // Assert

            var expectedResult = new List<Product>
            {
                new Product("a",2,1),
                new Product("s",2,1),
                new Product("z",2,1)
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Fact()]
        public void Should_Sort_Product_By_Descending()
        {
            // Arrange

            var target = new NameSorter(_products, SortDirection.Descending);

            // Act

            var result = target.Sort();

            // Assert

            var expectedResult = new List<Product>
            {
                new Product("z",2,1),
                new Product("s",2,1),
                new Product("a",2,1)
            };

            result.Should().BeEquivalentTo(expectedResult);
        }
    }
}