using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace ShoppingCart.Core.Entity.Sorter.Tests
{
    public class RecommendedSorterTests
    {
        private readonly List<Product> _products;
        private readonly Product _product1;
        private readonly Product _product2;
        private readonly Product _product3;
        private readonly Product _product4;
        private readonly Product _product5;

        public RecommendedSorterTests()
        {
            _product1 = new Product("a", 0, 1);
            _product2 = new Product("s", 0, 1);
            _product3 = new Product("z", 0, 1);
            _product4 = new Product("i", 0, 1);
            _product5 = new Product("p", 0, 1);

            _products = new List<Product> { _product1, _product2, _product3, _product4, _product5 };

            _product1.GetType().GetProperty("Popularity").SetValue(_product1, 4, null);
            _product2.GetType().GetProperty("Popularity").SetValue(_product2, 2, null);
            _product3.GetType().GetProperty("Popularity").SetValue(_product3, 5, null);
            _product4.GetType().GetProperty("Popularity").SetValue(_product4, null, null);
            _product5.GetType().GetProperty("Popularity").SetValue(_product5, 6, null);
        }

        [Fact()]
        public void Should_Return_Empty_List()
        {
            // Arrange

            var target = new RecommendedSorter(null);

            // Act

            var result = target.Sort();

            // Assert

            result.Should().NotBeNull();
            result.Should().HaveCount(0);
        }

        [Fact()]
        public void Should_Sort_Product_By_Ascending()
        {
            // Arrange

            var target = new RecommendedSorter(_products);

            // Act

            var result = target.Sort();

            // Assert

            var expectedResult = new List<Product>
            {
                _product2,
                _product1,
                _product3,
                _product5,
                _product4
            };

            result.Should().BeEquivalentTo(expectedResult);
        }
    }
}