using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace ShoppingCart.Core.Entity.Sorter.Tests
{
    public class PriceSorterTests
    {
        private readonly List<Product> _products;

        public PriceSorterTests()
        {
            _products = new List<Product>
            {
                new Product("a",1.1m,1),
                new Product("z",0.5m,1),
                new Product("s",0.7m,1)
            };
        }

        [Fact()]
        public void Should_Return_Empty_List()
        {
            // Arrange

            var target = new PriceSorter(null, SortDirection.Ascending);

            // Act

            var result = target.Sort();

            // Assert

            result.Should().NotBeNull();
            result.Should().HaveCount(0);
        }

        [Fact()]
        public void Should_Sort_Product_By_Ascending()
        {
            // Arrange

            var target = new PriceSorter(_products, SortDirection.Ascending);

            // Act

            var result = target.Sort();

            // Assert

            var expectedResult = new List<Product>
            {
                new Product("z",0.5m,1),
                new Product("s",0.7m,1),
                new Product("a",1.1m,1)
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Fact()]
        public void Should_Sort_Product_By_Descending()
        {
            // Arrange

            var target = new PriceSorter(_products, SortDirection.Descending);

            // Act

            var result = target.Sort();

            // Assert

            var expectedResult = new List<Product>
            {
                new Product("a",1.1m,1),
                new Product("s",0.7m,1),
                new Product("z",0.5m,1)
            };

            result.Should().BeEquivalentTo(expectedResult);
        }
    }
}