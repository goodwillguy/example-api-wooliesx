using FluentAssertions;
using ShoppingCart.Core.ValueObject;
using System.Collections.Generic;
using Xunit;

namespace ShoppingCart.Core.Entity.Tests
{
    public class PopularityCalculatorTests
    {
        private readonly List<ShoppingHistoryValueObject> _shoppingHistory;

        public PopularityCalculatorTests()
        {
            _shoppingHistory = new List<ShoppingHistoryValueObject>
            {
                new ShoppingHistoryValueObject
                {
                    CustomerId=1,
                    Products= new List<ProductValueObject> { new ProductValueObject { Name="1",Quantity=3} }
                },
                new ShoppingHistoryValueObject
                {
                    CustomerId=2,
                    Products= new List<ProductValueObject> { new ProductValueObject { Name="10",Quantity=2} }
                },
                 new ShoppingHistoryValueObject
                {
                    CustomerId=3,
                    Products= new List<ProductValueObject> { new ProductValueObject { Name="1",Quantity=1} , new ProductValueObject { Name = "10", Quantity = 4 } }
                }
            };
        }

        [Fact()]
        public void Should_Return_Max_Int_On_NoHistory()
        {
            // Arrange

            var target = new PopularityCalculator(null);

            // Act

            var result = target.ProductPopularity("2");

            // Assert

            result.Should().BeNull();
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("10", 0)]
        public void Should_Return_Popularity(string product, int expectedPopularity)
        {
            // Arrange

            var target = new PopularityCalculator(_shoppingHistory);

            // Act

            var result = target.ProductPopularity(product);

            // Asert

            result.Should().Be(expectedPopularity);
        }
    }
}