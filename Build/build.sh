dotnet  restore ./ShoppingCart.Sln
dotnet  build ./ShoppingCart.sln

dotnet test ./Tests/ShoppingCart.Api.AcceptanceTest/ShoppingCart.Api.AcceptanceTest.csproj
dotnet test ./Tests/ShoppingCart.Core.UnitTest/ShoppingCart.Core.UnitTest.csproj